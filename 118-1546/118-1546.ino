#include <LiquidCrystal_I2C.h>
//Initialize the Mux Shield
LiquidCrystal_I2C lcd(0x27,20,4);
int C120_7_38v = A1;
int C84_5v = A2;
int C80_5v = A3;
int C65_5v = A4;
int C76_3_3v = A5;
int C75_6v = A6;

//**Voltaje esperado*******
float V_Spctd;
float V_Rd;
float Global_Range = .2;
bool run_it = false;
bool Continue = true;

//******PIN SWITCH*********************
int InitButton = 1;
//******Out Init*********************
int Init_Out_V = 2;
//******LEDS**************
int Good_Part_Led = 3;
int Bad_Part_Led = 4;

String MsgOut;

int For_Count =0;
void setup() {
  pinMode(InitButton, INPUT_PULLUP);
  pinMode(Init_Out_V, OUTPUT);
  pinMode(Good_Part_Led, OUTPUT);
  pinMode(Bad_Part_Led, OUTPUT);  
  Serial.begin(9600);
  lcd.init();
}

void loop() {
    lcd.backlight();
  lcd.setCursor(0,0);
  lcd.print("Modelo: 118_3388");
  lcd.setCursor(0, 1);
  lcd.print("");
  run_it = !digitalRead(InitButton);
  Serial.println(run_it);
   if (run_it == true)  
 {
  MsgOut = "Iniciando Prueba...";
  Refresh_Msg();
  digitalWrite(Good_Part_Led, LOW);
  digitalWrite(Bad_Part_Led, LOW);
  digitalWrite(Init_Out_V, LOW);
  Continue = false;
  delay(1000);
  digitalWrite(Init_Out_V, HIGH);
  delay(500);
  
  //*******Test C120(7.38V)**********
  MsgOut = "Probando C120...";
  Refresh_Msg();
  Test_AnalogIn(7.38, C120_7_38v, Global_Range, "C120");

  //*******Test C84(5.0V)**********
  if (Continue==true){
    MsgOut = "Probando C84...";
    Refresh_Msg();
    Test_AnalogIn(5.0, C84_5v, Global_Range, "C84");
  }

  //*******Test C80(5.0V)**********
  if (Continue==true){
    MsgOut = "Probando C80...";
    Refresh_Msg();
    Test_AnalogIn(5.0, C80_5v, Global_Range, "C80");
  }
  
    //*******Test C65(5.0V)**********
  if (Continue==true){    
    MsgOut = "Probando C65...";
    Refresh_Msg();
    Test_AnalogIn(5.0, C65_5v, Global_Range, "C65");
  }

   //*******Test C76(3.3V)**********
   if (Continue==true){
    MsgOut = "Probando C76...";
    Refresh_Msg();
    Test_AnalogIn(3.3, C76_3_3v, Global_Range, "C76");
   }

  //*******Test C75(6.0V)**********
  if (Continue==true){
  MsgOut = "Probando C75...";
  Refresh_Msg();
  Test_AnalogIn(6.0, C75_6v, Global_Range, "C75");
  }
  if (Continue==true){
    MsgOut ="PIEZA OK";
    Refresh_Msg();
    Test_Ok_Led();
    Continue = false;    
  }
 }
}
 void Test_AnalogIn(float V_Need, int PIN, float Range, String TP_Msg){
   V_Spctd = V_Need;
   // CICLAR PRUEBA con un for
For_Count = 0;
   for (For_Count == 0; For_Count > 40;For_Count++){
     V_Rd= ((analogRead(PIN) * 5.0) /1023.0);
   //V_Rd = map(analogRead(PIN), 0, 1023, 0, 5);
    if( ((V_Spctd - Range) < (V_Rd)) && ((V_Rd)< (V_Spctd + Range ))){
      MsgOut = V_Rd + (String) (": OK");
      Continue = true;
      //Serial.println(V_Rd);
      Serial.println(MsgOut);
      Serial.println(V_Rd);
      Serial.println();
      Refresh_Msg();
      delay(50);

    }
    else {
      MsgOut = V_Rd + (String) (": ERR");
      Refresh_Msg();
      Continue = false;  
      //Serial.println(PIN);
      Serial.println(V_Rd);
      Serial.println();
      Test_Err_Led();  
      break;
      
    }
   }
       
 }
  void Test_Ok_Led(){
  digitalWrite(Good_Part_Led, HIGH);
  digitalWrite(Bad_Part_Led, LOW);
   digitalWrite(Init_Out_V, LOW);
  delay(500);
   while(!digitalRead(InitButton) == true){
    delay(2000);
   }
   Continue = false;
   run_it = false;
 }
  void Test_Err_Led(){
  digitalWrite(Good_Part_Led, LOW);
  digitalWrite(Bad_Part_Led, HIGH);
  delay(500);
  digitalWrite(Init_Out_V, LOW);
  Continue = false;
    while(!digitalRead(InitButton) == true){
    delay(2000);
   }
   Continue = false;
 run_it = false;
 }
  void Refresh_Msg (){
   lcd.setCursor(1, 1);
   lcd.print("                ");
   lcd.setCursor(1, 1);
   lcd.print(MsgOut);
 }
